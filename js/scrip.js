function BuscarPelicula() {
    const movieTitle = document.getElementById('movie-title').value;

    if (!movieTitle) {
        alert('NO HAS INGRESADO UN TITULO VALIDO!!');
        return;
    }
    const apiUrl = `http://www.omdbapi.com/?t=${movieTitle}&plot=full&apikey=30063268`;

    fetch(apiUrl)
        .then(response => response.json())
        .then(data => {
            // Verificar si se encontraron resultados
            if (data.Response === 'False') {
                alert('No se encontró información para la película ingresada.');
                return;
            }
            // Actualiza
            document.getElementById('title').textContent = `Nombre de la película: ${data.Title}`;
            document.getElementById('year').textContent = `Año de estreno: ${data.Year}`;
            document.getElementById('actors').textContent = `Actores principales: ${data.Actors}`;
            document.getElementById('plot').textContent = `Reseña: ${data.Plot}`;
            document.getElementById('poster').src = data.Poster;

            // Mostrar el mensaje de resultado
            document.getElementById('result-message').textContent = 'Resultado:';
        })
        .catch(error => {
            console.error('Error al obtener datos:', error);
            
        });
}